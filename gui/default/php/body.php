<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/9/14
 * Time: 1:38 AM
 * Project: application.local
 * File: body.php
 */
?>
<body>
<?php self::show(\maintenance\loader\Gui::body_header_template, $data); ?>
<?php self::show(\maintenance\loader\Gui::body_content_template, $data); ?>
<?php self::show(\maintenance\loader\Gui::body_footer_template, $data); ?>
</body>