<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/14/14
 * Time: 10:18 PM
 * Project: application.local
 * File: body_footer.php
 */
?>
<div class="mastfoot">
    <div class="inner">
        <p>Cover template for <a href="<?= $data->cover_link ?>"><?= $data->cover_link_ancor ?></a>, by <a
                href="https://twitter.com/mdo">@mdo</a>.</p>
    </div>
</div>

</div>

</div>

</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>
<script src="../../assets/js/docs.min.js"></script>
