<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/9/14
 * Time: 1:38 AM
 * Project: application.local
 * File: dom.php
 */
?>
<!DOCTYPE html>
<html lang="<?= \maintenance\loader\Config::LANGUAGE ?>">
<?php self::show(\maintenance\loader\Gui::head_template, $data); ?>
<?php self::show(\maintenance\loader\Gui::body_template, $data); ?>
</html>