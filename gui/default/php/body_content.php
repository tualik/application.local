<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/14/14
 * Time: 10:18 PM
 * Project: application.local
 * File: body_content.php
 */
?>
<div class="inner cover">
    <h1 class="cover-heading">Cover your page.</h1>

    <p class="lead">Cover is a one-page template for building simple and beautiful home pages. Download, edit the text,
        and add your own fullscreen background photo to make it your own.</p>

    <p class="lead">
        <a href="#" class="btn btn-lg btn-default">Learn more</a>
    </p>
</div>
