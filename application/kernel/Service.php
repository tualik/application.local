<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/9/14
 * Time: 3:55 AM
 * Project: application.local
 * File: Service.php
 */

namespace application\kernel;


class Service
{

    static $registry;
    static $event;

    function __construct($registry, $event)
    {
        self::$registry = $registry;
        self::$event = $event;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return mixed
     */
    public static function getEvent()
    {
        return self::$event;
    }

    /**
     * @param mixed $event
     */
    public static function setEvent($event)
    {
        self::$event = $event;
    }

    /**
     * @return mixed
     */
    public static function getRegistry()
    {
        return self::$registry;
    }

    /**
     * @param mixed $registry
     */
    public static function setRegistry($registry)
    {
        self::$registry = $registry;
    }
} 