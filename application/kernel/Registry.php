<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/9/14
 * Time: 3:19 AM
 * Project: application.local
 * File: Registry.php
 */

namespace application\kernel;


class Registry
{
    /*
            * @the vars array
            * @access private
            */
    private $vars = array();

    /**
     *
     * @get variables
     *
     * @param mixed $index
     *
     * @return mixed
     *
     */
    public function __get($index)
    {
        return $this->vars[$index];
    }

    /**
     *
     * @set undefined vars
     *
     * @param string $index
     *
     * @param mixed $value
     *
     * @return void
     *
     */
    public function __set($index, $value)
    {
        $this->vars[$index] = $value;
    }

    public function _set($array, $index, $value)
    {
        $this->vars[$array][$index] = $value;
        return true;
    }
} 