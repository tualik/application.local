<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/9/14
 * Time: 3:17 AM
 * Project: application.local
 * File: Kernel.php
 */

namespace application\kernel;


use application\service\data_preparing;
use application\service\data_processing;
use application\service\request_handlers;
use maintenance\loader\Boot;

class Kernel extends Service
{

    public static function start($request, $event)
    {
        self::load_registry();
        self::load_request($request, $event);
        self::initialize_service();
        // # processing -> registry->processing
        // # preparing  -> registry->preparing
        Boot::$data = self::getRegistry();
        Boot::$event = self::getEvent();
    }

    private static function load_registry()
    {
        self::$registry = new Registry();
        self::$registry->_set('debug', 'status', 'ok');
        self::$registry->_set('error', 'status', 'no_errors');
    }

    private static function load_request($request, $event)
    {
        self::$registry->_URI = $request["URI"]; /// '0'->handler_name   '1'->method_name    '2'-> from 2-above,-- method parameters in array
        self::$registry->_POST = $request["POST"]; /// if any POST data its here in array
        self::$registry->_GET = $request["GET"]; /// if any GET data its here in array
        self::$event = $event;
    }

    private static function initialize_service()
    {
        $handler = "application\\service\\request_handlers\\" . ucfirst(self::$registry->_URI[0]) . "Request";
        if (self::$registry->_URI[1] == "") {
            $action = "index";
        } else {
            $action = self::$registry->_URI[1];
        }
        $parameters = self::$registry->_URI;
        self::$registry->service = new $handler(self::$registry, self::$event);
        self::$registry->service->$action($parameters); /**/
    }
} 