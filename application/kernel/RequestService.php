<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/12/14
 * Time: 1:48 AM
 * Project: application.local
 * File: RequestService.php
 */

namespace application\kernel;


/**
 * Class RequestService
 * @package application\kernel
 */
class RequestService extends Service
{
    /**
     * @param $parameters
     */
    function load_processor($parameters)
    {
        $this->processing_instruction($parameters["instruction"]);
        $this->preparing_data($parameters["data"]);
    }

    /**
     * @param $instruction
     */
    function processing_instruction($instruction)
    {
        $this->load_processing($instruction);
    }

    /**
     * @param $data
     */
    function preparing_data($data)
    {
        $this->load_preparing($data);
    }

    /**
     * @param $instruction
     * @return null
     */
    public function load_processing($instruction)
    {
        if ($instruction != null) {
            $handler = $instruction["handler"];
            $action = $instruction["action"];
            $parameters = $instruction["parameters"];
            self::$registry->processing = new $handler(self::$registry, self::$event);
            self::$registry->processing->$action($parameters);
        } else {
            self::$registry->processing = null;
            return null;
        }
    }

    /**
     * @param $data
     * @return null
     */
    public function load_preparing($data)
    {
        if ($data != null) {
            $handler = $data["handler"];
            $action = $data["action"];
            $parameters = $data["parameters"];
            self::$registry->preparing = new $handler(self::$registry, self::$event);
            self::$registry->preparing->$action($parameters);
        } else {
            self::$registry->preparing = null;
            return null;
        }
    }
} 