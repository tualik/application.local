<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/12/14
 * Time: 2:22 AM
 * Project: application.local
 * File: Error_404Request.php
 */

namespace application\service\request_handlers;


use application\kernel\Request;
use application\kernel\RequestService;

class Error_404Request extends RequestService implements Request
{
    /*
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    */
    public function index($parameters)
    {
        self::$registry->_URI; /// '0'->handler_name   '1'->method_name    '2'-> from 2-above,-- method parameters in array
        self::$registry->_POST; /// if any POST data its here in array
        self::$registry->_GET; /// if any GET data its here in array
        #parameters('instructions('handler','action','parameters')');
        /*
         * handler    : object processor
         * action     : >>  doing action
         * parameters : parameters for instruction
         */
        #parameters('data('handler','action','parameters')');
        /*
         * handler    : object preparer
         * action     : >>  doing action
         * parameters : parameters for data
         */
        #$parameters["instruction"]
        #$parameters["data"]
        parent::load_processor($parameters);
    }
    /*
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    */
} 