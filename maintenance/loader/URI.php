<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/9/14
 * Time: 3:15 AM
 * Project: application.local
 * File: URI.php
 */

namespace maintenance\loader;


class URI
{

    public static function get_uri()
    {
        $URI = htmlspecialchars(trim($_SERVER['REQUEST_URI']));
        if (preg_match('/([^a-zA-Z0-9\.\/\-\_\#])/', $URI)) {
            ////// url syntax error
            Loader::$event["error"][] = "error: url syntax error message: input right URI or click on right link:";
            throw new \Exception('error: url segment count error message: input right URI or click on right link: ' . $URI);
        }
        $_URI = preg_split('/(\/|\..*$%)/', $URI, -1, PREG_SPLIT_NO_EMPTY);
        return $_URI;
    }

    public static function set_uri()
    {
        $_URI = self::get_uri();
        if (!empty($_URI)) {
            if (count($_URI) > Config::URL_SEGMENT_COUNT) {
                Loader::$event["error"][] = "error: url segment count error message: input right URI or click on right link:";
                throw new \Exception('error: url segment count error message: input right URI or click on right link: ' . count($_URI) . " > " . Config::URL_SEGMENT_COUNT);
            }
        } else {
            $_URI[0] = "index"; /// on index page
        }
        return $_URI;
    }
} 