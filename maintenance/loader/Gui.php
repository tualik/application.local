<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/9/14
 * Time: 3:13 AM
 * Project: application.local
 * File: Gui.php
 */

namespace maintenance\loader;


class Gui
{
    const dom_template = 'dom';
    const head_template = 'head';
    const body_template = 'body';
    const body_header_template = 'body_head';
    const body_content_template = 'body_content';
    const body_footer_template = 'body_footer';
    static $request;
    static $data;
    static $event;

    public static function gui($request, $data, $event)
    {
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        self::$request = $request;
        self::$data = $data;
        self::$event = $event;
        self::$data->gui = new Gui($request, $data, $event);
        self::show(Gui::dom_template, Gui::$data);
        //////////////////////////
        //var_dump(self::$request);
        //var_dump(self::$data);
        //var_dump(self::$event);
    }

    public static function show($template, $data)
    {
        $dir = 'php';
        Gui::including($dir, $template, $data);
    }

    public static function including($dir, $template, $data)
    {
        $path = GUI_DIR . '/' . $dir . '/' . $template . '.php';
        //echo ">>>>>>>>".$path;
        if (file_exists($path) == false) {
            //throw new Exception('Template not found in ' . $path);
            throw new \Exception('Template not found in ' . $path);
        } else {
            include($path);
            return true;
        }
    }

    public function _show($template)
    {
        $data = self::$data;
        $dir = 'tpl';
        Gui::including($dir, $template, $data);
    }

    public function _foreach($dir, $template, $foreach)
    {
        $count = count($foreach);
        for ($i = 0; $i < $count; $i++) {
            Gui::including($dir, $template, self::$data);
        }
    }

} 