<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/9/14
 * Time: 3:14 AM
 * Project: application.local
 * File: Loader.php
 */

namespace maintenance\loader;


class Loader
{
    public static $request;
    public static $event;
    public static $URL;
    public static $POST;
    public static $GET;


    public static function initialize()
    {
        self::get_uri();
        self::post_sanitizer();
        self::get_sanitizer();
        self::check_request();
        Boot::$request = self::$request;
        Boot::$event = self::$event;
        Boot::initialize();
        //var_dump(self::$request);
        //var_dump(self::$event);

    }

    public static function get_uri()
    {
        self::$URL = self::$request["URI"] = URI::set_uri();
    }

    public static function post_sanitizer()
    {
        if (empty($_POST)) {
            self::$POST = self::$request["POST"] = null;
            return null;
        } else {
            foreach ($_POST as $key => $value) {
                self::$request["POST"][] = array(self::sanitize($key) => self::sanitize($value));
            }
            self::$POST = self::$request["POST"];
            unset($_POST);
            return true;
        }
    }

    public static function sanitize($data)
    {
        $sanitize = htmlspecialchars(trim($data));
        if (preg_match('/([^a-zA-Z0-9\.\/\-\_\#])/', $sanitize)) {
            Loader::$event["error"][] = "error: data error message: input data filtering error:";
            throw new \Exception('error: data error message: input data filtering error: in ' . $data);
        }
        return $sanitize;
    }

    public static function get_sanitizer()
    {
        if (empty($_GET)) {
            self::$GET = self::$request["GET"] = null;
            return null;
        } else {
            foreach ($_GET as $key => $value) {
                self::$request["POST"][] = array(self::sanitize($key) => self::sanitize($value));
            }
            self::$GET = self::$request["GET"];
            unset($_GET);
            return true;
        }
    }

    public static function check_request()
    {
        //// is there request file
        //// is there processing file
        //// is there preparing file
        //// is there template file
        //// is there correct uri if no one was founded
        //// if uri or request are incorrect,- handle as required, send kernel error or event processing right request
    }
} 