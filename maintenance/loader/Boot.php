<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/9/14
 * Time: 3:14 AM
 * Project: application.local
 * File: Boot.php
 */

namespace maintenance\loader;


use application\kernel\Kernel;

class Boot
{
    static $request;
    static $data;
    static $event;

    public static function initialize()
    {
        Kernel::start(self::$request, self::$event);
        Gui::gui(self::$request, self::$data, self::$event);


        //////////////////////////////////////////////////////
        echo '<table border="2"><th>$REQUEST</th><tr><td>';
        var_dump(self::$request);
        echo '</td></tr><th>$DATA</th><tr><td>';
        var_dump(self::$data);
        echo '</td></tr><th>$EVENT</th><tr><td>';
        var_dump(self::$event);
        echo "</td></tr></table>";
    }
}