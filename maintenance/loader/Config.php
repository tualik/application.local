<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/9/14
 * Time: 3:15 AM
 * Project: application.local
 * File: Config.php
 */

namespace maintenance\loader;


class Config
{

    public static $time;
    public static $config = array();
    const HOST = 'application.local';
    const URL_SEGMENT_COUNT = '8';
    const TEMPLATE = 'default';
    const LANGUAGE = 'en';

    public static function start_timer()
    {
        self::$time = self::get_real_time();
    }

    public static function get_real_time()
    {
        list ($seconds, $microSeconds) = explode(' ', microtime());
        return (( float )$seconds + ( float )$microSeconds);
    }

    public static function stop_timer()
    {
        return round((self::get_real_time() - self::$time), 5);
    }

    public static function define_data()
    {

    }

    public static function LogToFile($name, $data)
    {
        $dir = LOADER_DIR . DIRSEP . "log.html";
        $file = fopen($dir, 'a');
        flock($file, LOCK_EX);
        fwrite($file, ('<table border="1"><th colspan="2"> ' . $name . ' </th><tr><td> ' . $data . ' </td><td> => ' . date('d.m.Y H:i:s') . ' </td></tr>' . PHP_EOL));
        flock($file, LOCK_UN);
        fclose($file);
    }


    public static function make_file_paths()
    {
        define ('APPLICATION_DIR', ROOT_DIR . DIRSEP . 'application');
        define ('KERNEL_DIR', ROOT_DIR . DIRSEP . 'application' . DIRSEP . 'kernel');
        define ('LIBRARY_DIR', ROOT_DIR . DIRSEP . 'application' . DIRSEP . 'library');
        define ('DATA_PREPARING_DIR', ROOT_DIR . DIRSEP . 'application' . DIRSEP . 'service' . DIRSEP . 'data_preparing');
        define ('DATA_PROCESSING_DIR', ROOT_DIR . DIRSEP . 'application' . DIRSEP . 'service' . DIRSEP . 'data_processing');
        define ('REQUEST_HANDLERS_DIR', ROOT_DIR . DIRSEP . 'application' . DIRSEP . 'service' . DIRSEP . 'request_handlers');
        define ('MAINTENANCE_DIR', ROOT_DIR . DIRSEP . 'maintenance');
        define ('DEBUGGING_DIR', ROOT_DIR . DIRSEP . 'maintenance' . DIRSEP . 'debugging');
        define ('ERROR_HANDLER_DIR', ROOT_DIR . DIRSEP . 'maintenance' . DIRSEP . 'error_handler');
        define ('LOADER_DIR', ROOT_DIR . DIRSEP . 'maintenance' . DIRSEP . 'loader');
        define ('GUI_DIR', ROOT_DIR . DIRSEP . 'gui' . DIRSEP . Config::TEMPLATE);
        spl_autoload_register(array('\maintenance\loader\Config', 'autoload'));
        self::define_data();
    }

    public static function make_object($directory, $prefix, $postfix, $namespace, $class_name, $class_registry, $class_event, $method, $method_params)
    {
        if (file_exists($directory . DIRSEP . $prefix . ucfirst($class_name) . $postfix)) {
            $class = $namespace . $prefix . ucfirst($class_name) . $postfix;
            //echo $class . "<<<<<<<<<<<<<<<<<<<<<<<<<";
            $method = ucfirst($method);
            if (method_exists($class, $method)) {
                $object = array();
                $object["object"] = new $class($class_registry, $class_event);
                $object["result"] = $object["object"]->$method($method_params);
            } else {
                return $object = "no method found";
            }
        } else {
            return $object = "no class found";
        }
        return $object;
    }

    public static function make_class_name($prefix, $name, $postfix)
    {
        $prefix = strtolower($prefix);
        $name = strtolower($name);
        $postfix = strtolower($postfix);
        $concatenation = ucfirst($prefix) . ucfirst($name) . ucfirst($postfix);
        return $concatenation;
    }

    public static function check_class($class_name)
    {

    }

    public static function check_method($class_name, $method_name)
    {
        if (method_exists($class_name, $method_name)) {
            $check_method = "exist";
        } else {
            $check_method = "not_exist";
        }
        return $check_method;
    }

    public static function autoload($class_name)
    {
        //echo "<p>class file > " . $class_name . " </p>";
        $path_file = explode(',', str_replace("\\", " , ", $class_name));
        $_file = trim($path_file[count($path_file) - 1]);
        $_dir[0] = KERNEL_DIR . DIRSEP;
        $_dir[1] = LIBRARY_DIR . DIRSEP;
        $_dir[2] = DATA_PREPARING_DIR . DIRSEP;
        $_dir[3] = DATA_PROCESSING_DIR . DIRSEP;
        $_dir[4] = REQUEST_HANDLERS_DIR . DIRSEP;
        $_dir[5] = DEBUGGING_DIR . DIRSEP;
        $_dir[6] = ERROR_HANDLER_DIR . DIRSEP;
        $_dir[7] = LOADER_DIR . DIRSEP;
        $_dir[8] = GUI_DIR . DIRSEP;
        $count = count($_dir);
        for ($i = 0; $i < $count; $i++) {
            if (file_exists($_dir[$i] . $_file . ".php") == true) {
                $real_dir = $_dir[$i];
                break;
            }
        }
        if (!empty($real_dir)) {
            $extension = '.php';
            require_once $real_dir . $_file . $extension;
            //echo $real_dir . $_file . '.php';
            return $real_dir . $_file . '.php';
        }
        return array();
    }

    public static function Start_Diagnostics()
    {
        if (!defined('E_DEPRECATED')) {
            error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
            ini_set('error_reporting', E_ALL ^ E_WARNING ^ E_NOTICE);
        } else {
            error_reporting(E_ALL ^ E_WARNING ^ E_DEPRECATED ^ E_NOTICE);
            ini_set('error_reporting', E_ALL ^ E_WARNING ^ E_DEPRECATED ^ E_NOTICE);
        }
        ini_set('display_errors', true);
        ini_set('html_errors', true);
    }

    public static function Start_Caching()
    {
        ob_start();
        ob_implicit_flush(1);
    }
} 