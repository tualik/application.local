<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/9/14
 * Time: 12:28 AM
 * Project: application.local
 * File: index.php
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define ('DEBUGGING_MODE', true);
define ('INDEX_HOST', 'our_script');
define ('DIRSEP', '/');
define ('ROOT_DIR', dirname(__FILE__));
require_once ROOT_DIR . DIRSEP . "/maintenance/loader/Config.php";
\maintenance\loader\Config::start_timer();
\maintenance\loader\Config::Start_Diagnostics();
\maintenance\loader\Config::Start_Caching();
\maintenance\loader\Config::make_file_paths();
\maintenance\loader\Loader::initialize();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (empty($_POST)) {
    echo "<p>post is empty</p>";
} else {
    echo "<p>there is something in post</p>";
}
if (empty($_GET)) {
    echo "<p>get is empty</p>";
} else {
    echo "<p>there is something in post</p>";
}
echo "<hr>";
//\maintenance\loader\Config::LogToFile('Generation', \maintenance\loader\Config::stop_timer());
echo \maintenance\loader\Config::stop_timer();